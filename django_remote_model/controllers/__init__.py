from .remote_model import RemoteModel
from .remote_model import RemoteQuerySet

__all__ = ['RemoteModel', 'RemoteQuerySet']
