from ..helpers.router import app_router, app_exists
from ..helpers import exceptions


class RemoteQuerySetIterator:
    ''' Iterator class '''

    def __init__(self, model):
        self.__model = model
        self.__index = 0

    def __next__(self):
        if self.__index < len(self.__model.data):
            if self.__index < len(self.__model.data):  # Check if reached the end
                result = self.__model[self.__index]
            self.__index += 1
            return result
        # End of Iteration
        raise StopIteration


class RemoteQuerySet:
    __remote_app = None
    __remote_model = None
    __page_size = None
    __count = None
    __response = None
    __response_data = None
    __primary_key = None
    __rec = {}
    __is_new = False

    data = None
    items = None

    def __init__(self, remote_app, remote_model, page_size=None, data={}, primary_key_field=None, is_new=False):
        self.__remote_app = remote_app
        self.__remote_model = remote_model
        self.__page_size = page_size
        self.__rec = data
        self.__primary_key = primary_key_field
        self.__is_new = is_new

        if not app_exists(remote_app):
            raise Exception(f"Couldn't find '{remote_app}' in REMOTE_APPS")

    def __iter__(self):
        return RemoteQuerySetIterator(self)

    # Read only properties:
    @property
    def remote_app(self):
        return self.__remote_app

    @property
    def remote_model(self):
        return self.__remote_model

    @property
    def count(self):
        return self.__count

    @property
    def data(self):
        if self.__rec:
            return self.__rec
        if 'data' in (self.__response_data or {}):
            return self.__response_data['data']
        return None

    @property
    def items(self):
        if self.__rec:
            raise Exception('Cannot get items in single record dataset')
        if 'data' in (self.__response_data or {}):
            return self.__response_data['data']
        return []

    # Public methods:
    def all(self, page=None, page_size=None):
        return self.filter(page=page, page_size=page_size)

    def filter(self, page=None, page_size=None, **kwargs):
        self.__filter = kwargs
        params = {
            'page': page,
            'page_size': page_size or self.__page_size,
            **self.__filter
        }
        print('params', params)
        self.__response = self.__remote_request('get', params=params)
        try:
            self.__response_data = self.__response.json()
        except Exception:
            self.__response_data = None
        if 'data' in self.__response_data:
            self.__count = self.__response_data['count']
            self.__primary_key = self.__response_data.get('primary_key', None)

            return self
        else:
            ResponseError = Exception
            if self.__response.status_code == 403:
                ResponseError = exceptions.PermissionDenied

            if 'error' in self.__response_data:
                raise ResponseError(self.__response_data['error'])
            elif 'detail' in self.__response_data:
                raise ResponseError(self.__response_data['detail'])

    def get(self, **kwargs):
        # TODO: Implement get on server side for performance!
        self.filter(**kwargs)

        if self.count == 0:
            raise exceptions.DoesNotExist(f'{self.__remote_app}.{self.__remote_model} matching query does not exist.')
        if self.count > 1:
            raise exceptions.MultipleObjectsReturned(f'get() returned more than one {self.__remote_app}.{self.__remote_model} -- it returned {self.count}!')

        self.__set_rec(self.first().data)

        return self

    def item(self, index):
        if self.__rec:
            raise Exception('Cannot get item in single record dataset')
        return RemoteQuerySet(
            remote_app=self.__remote_app,
            remote_model=self.__remote_model,
            page_size=self.__page_size,
            data=self.items[index],
            primary_key_field=self.__primary_key
        )

    def save(self):
        data = self.__rec
        pk_field = self.__primary_key
        pk = None

        if not data:
            raise Exception('Cannot save item in multi record dataset')

        if self.__is_new:
            self.__response = self.__remote_request('post', data=self.__rec)
        else:
            if not pk_field:
                raise Exception('Cannot save without primary_key, dataset is read only')

            if pk_field not in data.keys():
                raise Exception(f'The primary_key [{pk_field}] is not present in the dataset')

            pk = data[pk_field]
            self.__response = self.__remote_request('put', data=self.__rec, url_parts=[str(pk)])

        if self.__response.status_code != 201:
            try:
                error = self.__response.json()['error']
            except Exception:
                error = 'Item could not be created'
            raise Exception(error)

        self.__set_rec(self.__response.json()['data'])
        self.__is_new = False
        return self

    def delete(self):
        data = self.__rec
        pk_field = self.__primary_key
        pk = None

        if not data:
            raise Exception('Cannot delete item in multi record dataset')

        if self.__is_new:
            raise Exception('Cannot delete unfetched item')

        if not pk_field:
            raise Exception('Cannot delete item without primary_key, dataset is read only')

        if pk_field not in data.keys():
            raise Exception(f'The primary_key [{pk_field}] is not present in the dataset')

        pk = data[pk_field]

        self.__response = self.__remote_request('delete', url_parts=[str(pk)])

        if self.__response.status_code != 204:
            try:
                error = self.__response.json()['error']
            except Exception:
                error = 'Item could not be deleted'
            raise Exception(error)

        self.__clear_rec()

    def first(self):
        try:
            return self.item(0)
        except IndexError:
            return None

    def last(self):
        try:
            return self.item(-1)
        except IndexError:
            return None

    # Private methods:
    def __remote_request(self, action, url_parts=[], params={}, data={}, **kwargs):
        endpoint = f'remote-model/model/{self.__remote_model}/{str.join("/",url_parts)}{"/" if url_parts else ""}'

        return app_router(self.__remote_app, endpoint, action, params=params, data=data, **kwargs)

    def __clear_rec(self):
        # self.__rec.clear()
        self.__rec = {}.copy()

    def __set_rec(self, data):
        self.__clear_rec()
        if not data:
            return
        for k, v in data.items():
            self.__rec[k] = v

    # Override native methods:
    def __len__(self):
        return len(self.items)

    def __getitem__(self, index):
        return self.item(index)

    def __getattr__(self, key):
        rec = self.data
        if not rec:
            return super().__getattribute__(key)
        try:
            return rec[key]
        except KeyError:
            return super().__getattribute__(key)

    def __setattr__(self, key, val):
        try:
            rec = self.__rec
            rec[key]  # ensure that key exists in record
            if key == self.__primary_key:
                raise Exception(f'Primary key field [{key}] is read only')
            rec[key] = val
            return val
        except KeyError:
            return super().__setattr__(key, val)

    def __delattr__(self, key):
        try:
            rec = self.__rec
            rec[key]
            return rec.__delitem__(key)
        except KeyError:
            return super().__delattr__(key)

    def __dir__(self):
        return [*super().__dir__(), *self.__rec.keys()]

    def __str__(self):
        if self.__rec:
            return str(self.__rec)
        return super().__str__()


class RemoteModel:
    __slots__ = ['__remote_app', '__remote_model', '__page_size']

    def __init__(self, remote_app, remote_model, page_size=None):
        self.__remote_app = remote_app
        self.__remote_model = remote_model
        self.__page_size = page_size

    def __call__(self, **kwargs):
        return RemoteQuerySet(remote_app=self.__remote_app, remote_model=self.__remote_model, data=kwargs, page_size=self.__page_size, is_new=True)

    @property
    def objects(self):
        return RemoteQuerySet(remote_app=self.__remote_app, remote_model=self.__remote_model, page_size=self.__page_size)
