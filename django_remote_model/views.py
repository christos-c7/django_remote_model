from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from .helpers.authentication import get_authentication_class
from .helpers.model import has_action_access, get_serializer, get_model, get_model_filter, get_model_primary_key
from .helpers.router import parse_query_params, get_app_name_from_request
from django.db.models import Q
import traceback
import json


class RemoteModelPaginator(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return {
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'primary_key': None,
            'count': self.page.paginator.count,
            'data': data
        }


class RemoteModelViewSet(viewsets.ModelViewSet):
    queryset = None
    serializer_class = None
    pagination_class = RemoteModelPaginator

    def __init__(self):
        self.authentication_classes = get_authentication_class()

    def list(self, request, *args, **kwargs):
        try:
            model_definition_name = kwargs['model_definition_name']
            app = get_app_name_from_request(request)

            if not has_action_access(model_definition_name, 'list', app):
                return self.__error_response(f"App {app} has no access to '{model_definition_name}' list action", 403)

            serializer = get_serializer(model_definition_name)
            model = get_model(model_definition_name)
            params = parse_query_params(request.query_params)
            filter = get_model_filter(model_definition_name)

            primary_key = get_model_primary_key(model)

            query = Q(**params)
            if filter:
                # apply server-side filter:
                query = Q(Q(**filter) & Q(**params))

            queryset = model.objects.filter(query)

            page_param = request.query_params.get('page', None)
            if page_param is None:
                # Without pagination:
                response = serializer(queryset, many=True)
                return Response({
                    'primary_key': primary_key,
                    'count': len(response.data),
                    'data': response.data
                })
            else:
                # With pagination:
                try:
                    paginated_queryset = self.paginate_queryset(queryset)
                except Exception:
                    return self.__error_response('Invalid page', 404)

                response = serializer(paginated_queryset, many=True)

                paginated_response = self.get_paginated_response(response.data)
                paginated_response['primary_key'] = primary_key
                return Response(paginated_response)
        except Exception as e:
            traceback.print_exc()
            return self.__error_response(str(e), 404)

    def create(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        if not has_action_access(model_definition_name, 'create', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' create action", 403)

        try:
            data = self.__get_data(request)
        except Exception as e:
            return self.__error_response(str(e), 400)

        serializer = get_serializer(model_definition_name)(data=data)
        if not serializer.is_valid():
            return self.__error_response(serializer.errors, 400)

        serializer.save()

        return Response(data={'data': serializer.data}, status=201)

    def retrieve(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        if not has_action_access(model_definition_name, 'retrieve', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' retrieve action", 403)

        model = get_model(model_definition_name)

        try:
            queryset = model.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return self.__error_response(str(e), 404)

        serializer = get_serializer(model_definition_name)
        response = serializer(queryset)

        return Response({
            'data': response.data
        })

    def retrieve_one(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        params = parse_query_params(request.query_params)
        filter = get_model_filter(model_definition_name)

        query = Q(**params)
        if filter:
            # apply server-side filter:
            query = Q(Q(**filter) & Q(**params))

        if not has_action_access(model_definition_name, 'retrieve', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' retrieve action", 403)

        model = get_model(model_definition_name)

        try:
            queryset = model.objects.get(query)
        except Exception as e:
            return self.__error_response(str(e), 404)

        serializer = get_serializer(model_definition_name)
        response = serializer(queryset)

        return Response({
            'data': response.data
        })

    def update(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        if not has_action_access(model_definition_name, 'update', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' update action", 403)

        model = get_model(model_definition_name)

        try:
            queryset = model.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return self.__error_response(str(e), 404)

        try:
            data = self.__get_data(request)
        except Exception as e:
            return self.__error_response(str(e), 400)

        serializer = get_serializer(model_definition_name)(queryset, data=data)

        if not serializer.is_valid():
            return self.__error_response(serializer.errors, 400)

        serializer.save()

        return Response(data={'data': serializer.data}, status=201)

    def partial_update(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        if not has_action_access(model_definition_name, 'partial_update', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' partial_update action", 403)

        model = get_model(model_definition_name)

        try:
            queryset = model.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return self.__error_response(str(e), 404)

        try:
            data = self.__get_data(request)
        except Exception as e:
            return self.__error_response(str(e), 400)

        serializer = get_serializer(model_definition_name)(queryset, data=data, partial=True)

        if not serializer.is_valid():
            return self.__error_response(serializer.errors, 400)

        serializer.save()

        return Response(data={'data': serializer.data}, status=201)

    def destroy(self, request, *args, **kwargs):
        model_definition_name = kwargs['model_definition_name']
        app = get_app_name_from_request(request)

        if not has_action_access(model_definition_name, 'destroy', app):
            return self.__error_response(f"App {app} has no access to '{model_definition_name}' destroy action", 403)

        model = get_model(model_definition_name)

        try:
            queryset = model.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return self.__error_response(str(e), 404)

        self.perform_destroy(queryset)

        return Response(status=204)

    def __error_response(self, message, status_code):
        return Response({
            'error': message
        }, status=status_code)

    def __get_data(self, request):
        data = request.data
        if not data:
            raise Exception('No data payload sent')
        try:
            data = json.loads(data['d'])
        except KeyError:
            raise Exception('Wrong data payload in request')
        return data
