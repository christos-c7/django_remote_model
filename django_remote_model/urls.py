from django.conf.urls import url, include
from . import views

endpoint_patterns = [
    url(r'^model/(?P<model_definition_name>\w+)/$', views.RemoteModelViewSet.as_view(actions={'get': 'list', 'post': 'create'}), name='remote_model'),
    url(r'^model/(?P<model_definition_name>\w+)/(?P<pk>\d+)/$', views.RemoteModelViewSet.as_view(actions={'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}), name='remote_model_get_by_id'),
    url(r'^model/(?P<model_definition_name>\w+)/get/$', views.RemoteModelViewSet.as_view(actions={'get': 'retrieve_one'}), name='remote_model_get')
]

urlpatterns = [
    url(r'^remote-model/', include(endpoint_patterns))
]
