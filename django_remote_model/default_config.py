# Remote Application Services:
REMOTE_APPS = {}

# The imported serializers file:
SERIALIZERS = None

# The authentication header to access remote apps:
AUTH_HEADER = {}

# The local app that the models will be loaded from:
TARGET_APP = None

# Local Model Configuration:
MODEL_CONFIG = {}
