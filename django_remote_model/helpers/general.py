from .. import default_config
from django.conf import settings

CONFIG_MODULE = getattr(settings, 'REMOTE_MODEL_CONFIG', None)


def configuration():
    if CONFIG_MODULE:
        return __import__(CONFIG_MODULE, fromlist=[None])
    return default_config
