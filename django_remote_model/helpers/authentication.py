from rest_framework import authentication
from rest_framework import exceptions
from django.contrib.auth.models import AnonymousUser
from ..helpers.general import configuration
from pydoc import locate

config = configuration()


class DefaultAuth(authentication.BaseAuthentication):

    def authenticate(self, request):

        REMOTE_APPS = getattr(config, 'REMOTE_APPS', [])

        for remote_app in REMOTE_APPS:
            app = REMOTE_APPS[remote_app]

            auth_header = app.get('auth-header')
            secret_key = app.get('secret-key')

            if auth_header and secret_key:
                secret = request.META.get(auth_header, None)
                # Check if valid auth header is passed:
                if secret and secret == secret_key:
                    return AnonymousUser, None

        raise exceptions.AuthenticationFailed('Could not authenticate.')


def get_authentication_class():
    try:
        AUTHENTICATION_CLASS_PATH = getattr(config, 'AUTHENTICATION_CLASS_PATH', None)

        if AUTHENTICATION_CLASS_PATH:
            return [locate(AUTHENTICATION_CLASS_PATH)]

    except Exception as e:
        print('get_authentication_class - error', str(e))

    return [DefaultAuth]
