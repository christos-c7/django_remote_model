from django.apps import apps
from rest_framework.serializers import ModelSerializer
from ..helpers.general import configuration
from pydoc import locate

config = configuration()


def has_action_access(model, action, app):
    try:
        apps = config.MODEL_CONFIG.get(model).get('actions').get(action)
    except Exception:
        return False

    if not apps or app not in apps:
        return False

    return True


def get_model_filter(model_definition_name):
    return config.MODEL_CONFIG.get(model_definition_name).get('filter', None)


def get_model_primary_key(model):
    try:
        return model._meta.pk.name
    except Exception:
        return None


def get_model(model_definition_name):
    model_definition = config.MODEL_CONFIG.get(model_definition_name)
    if not model_definition:
        raise Exception(f"Cannot find definition for model {model_definition_name}. Check remote APP's configuration.")
    model_name = model_definition.get('model_class')
    target_app = model_definition.get('target_app', config.TARGET_APP)

    return apps.get_model(app_label=target_app, model_name=model_name)


def get_serializer(model_definition_name):
    serializer = __get_serializer(model_definition_name)
    if not serializer:
        raise Exception(f"Cannot find serializer for model {model_definition_name}. Check remote APP's configuration.")

    return serializer


def __get_serializer(model_definition_name):
    # Check if specific serializer path is set and return the class:
    serializer_path = config.MODEL_CONFIG.get(model_definition_name).get('serializer_path', None)
    if serializer_path:
        return locate(serializer_path)

    # Get serializer class name:
    serializer_class = config.MODEL_CONFIG.get(model_definition_name).get('serializer_class')

    # If there's no serializer, return a default one:
    if not serializer_class:
        model_class = get_model(model_definition_name)

        class DefaultSerializer(ModelSerializer):
            class Meta:
                model = model_class
                fields = '__all__'

        return DefaultSerializer

    # Check if SERIALIZERS_PATH is set and return the class:
    SERIALIZERS_PATH = getattr(config, 'SERIALIZERS_PATH', None)
    if SERIALIZERS_PATH:
        serializer_path = f'{SERIALIZERS_PATH}.{serializer_class}'
        return locate(serializer_path)

    # Check if SERIALIZERS file is imported and return the class:
    SERIALIZERS = getattr(config, 'SERIALIZERS', None)
    if SERIALIZERS:
        return getattr(SERIALIZERS, serializer_class)

    return None
