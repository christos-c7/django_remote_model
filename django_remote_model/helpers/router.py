import requests
import json
from ..helpers.general import configuration

config = configuration()


def parse_query_params(query_params):
    result = {}
    for key, value in query_params.items():
        if key in ['page', 'page_size']:
            continue
        result[key] = value

    return result


def get_app_name_from_request(request):
    for app in config.REMOTE_APPS:
        app_auth_header = config.REMOTE_APPS[app].get('auth-header')
        if request.META.get(app_auth_header):
            return app
    return None


def get_app_baseurl(app):
    return config.REMOTE_APPS.get(app).get('base-url')


def app_exists(app):
    return app in config.REMOTE_APPS


def app_router(app, endpoint, action, params={}, data={}, **kwargs):
    headers = {
        **config.AUTH_HEADER,
        **(kwargs.get('headers') or {})
    }
    kwargs['headers'] = headers

    baseurl = get_app_baseurl(app)
    endpoint = baseurl + endpoint

    print('endpoint', endpoint)

    json_data = {"d": json.dumps(data)}

    request_action = getattr(requests, action)
    response = request_action(endpoint, params=params, json=json_data, **kwargs)

    print('response', response)

    return response
