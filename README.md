kn-remote-model
==================================
[![Build Status](https://travis-ci.org/aykut/django-bulk-update.svg?branch=master)](https://travis-ci.org/aykut/django-bulk-update)
[![Coverage Status](https://coveralls.io/repos/aykut/django-bulk-update/badge.svg?branch=master)](https://coveralls.io/r/aykut/django-bulk-update?branch=master)

Use Django ORM remotely from another backend Django app.


Installation
==================================
    pip install remote_model

Usage
==================================
With manager:

```python
from django_remote_model import RemoteModel

model = RemoteModel('remote_app_name', 'RemoteModelName')

model.objects.filter(id=1234)

item1 = model[0]

...
```

Requirements
==================================
- Django 1.8+


TODO
==================================
- Geometry Fields support

License
==================================
remote-model is released under the MIT License. See the LICENSE file for more details.
